# 连锁门店管理系统

#### 介绍
1. vue+springboot的连锁门店管理系统
2. 后端仓库：https://gitee.com/dyuloon/salesms_springboot

此系统是基于 VUE2全家桶 + elementUI + ECharts/DataV + axios 的前后端分离式管理系统，在疫情背景下，为小型连锁门店管理者提供在线员工管理与可视化业绩展示，提供决策者所需的数据参考
此系统技术栈为SpringBoot + MyBatis-Plus + MySq l开发后端接口；Vue2 + VueCLI + ElementUI + ECharts/DataV 开发前端web + 可视化界面；外加Gitee多人协作开发

#### 项目亮点
1、动态路由设置
2、用户权限控制
3、数据可视化
4、前后端分离
5、基于gitee的版本控制和多人团队协作开发
6、前端组件化开发
7、封装axios异步请求，api统一管理

![输入图片说明](public/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230828093348.png)

![输入图片说明](public/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230828093254.png)

![输入图片说明](public/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230828093302.png)

![输入图片说明](public/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230828093309.png)

![输入图片说明](public/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230828093330.png)

![输入图片说明](public/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230828093253.png)

![输入图片说明](public/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230828093430.png)